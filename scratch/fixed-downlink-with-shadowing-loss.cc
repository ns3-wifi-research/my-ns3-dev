#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/buildings-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FixedDownlinkWithShadowingLoss");

int main(int argc, char *argv[]){
    /* Set time resolution to nanoseconds */
    Time::SetResolution(Time::NS);

    /* Enable log messages */
    LogComponentEnable("FixedDownlinkWithShadowingLoss", LOG_INFO);

    /* Enable checksums */
    GlobalValue::Bind("ChecksumEnabled", BooleanValue(true));

    /* Create the Nodes */
    NS_LOG_INFO("Creating Nodes...");
    NodeContainer wifiApNodes;
    wifiApNodes.Create(1);
    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(1);

    /* Create the wifi channel */
    NS_LOG_INFO("Setting up the channel...");
    WifiHelper wifi;
    wifi.SetStandard(WIFI_PHY_STANDARD_80211n_2_4GHZ);
    wifi.SetRemoteStationManager("ns3::MinstrelHtWifiManager");

    YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
    YansWifiChannelHelper wifiChannel;

    wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");

    /* Add indoor propagation loss */
    Ptr<Building> building = CreateObject<Building>();
    building->SetBoundaries(Box(0.0, 100.0, 0.0, 100.0, 0.0, 10.0));
    building->SetBuildingType(Building::Office);
    building->SetExtWallsType(Building::ConcreteWithWindows);
    building->SetNFloors(1);
    building->SetNRoomsX(1);
    building->SetNRoomsY(1);

    MobilityHelper apMobility;
    apMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    Ptr<ListPositionAllocator> apPosition = CreateObject<ListPositionAllocator>();
    apPosition->Add(Vector(0.0, 0.0, 1.0));
    apMobility.SetPositionAllocator(apPosition);
    apMobility.Install(wifiApNodes);
    BuildingsHelper::Install(wifiApNodes);

    MobilityHelper staMobility;
    staMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    Ptr<ListPositionAllocator> staPosition = CreateObject<ListPositionAllocator>();
    staPosition->Add(Vector(30.0, 0.0, 1.0));
    staMobility.SetPositionAllocator(staPosition);
    staMobility.Install(wifiStaNodes);
    BuildingsHelper::Install(wifiStaNodes);

    BuildingsHelper::MakeMobilityModelConsistent();

    wifiChannel.AddPropagationLoss(
        "ns3::HybridBuildingsPropagationLossModel",
        "Frequency", DoubleValue(2.4*1e9),
        "ShadowSigmaOutdoor", DoubleValue(5.0),
        "ShadowSigmaIndoor", DoubleValue(5.0),
        "ShadowSigmaExtWalls", DoubleValue(5.0)
    );

    wifiPhy.SetChannel(wifiChannel.Create());

    WifiMacHelper wifiMac;
    Ssid ssid = Ssid("ns3");

    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiApDevices = wifi.Install(wifiPhy, wifiMac, wifiApNodes.Get(0));

    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiStaDevices = wifi.Install(wifiPhy, wifiMac, wifiStaNodes.Get(0));

    /* Install the internet stack */
    NS_LOG_INFO("Installing the internet stack...");
    InternetStackHelper internet;
    internet.Install(wifiApNodes);
    internet.Install(wifiStaNodes);

    /* Assign the IPV4 addresses */
    NS_LOG_INFO("Assigning the IP addresses...");
    Ipv4AddressHelper address;
    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiApInterfaces = address.Assign(wifiApDevices);
    Ipv4InterfaceContainer wifiStaInterfaces = address.Assign(wifiStaDevices);

    /* Setup the applications */
    NS_LOG_INFO("Setting up the applications...");
    uint16_t port = 9;
    PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    ApplicationContainer apps_sink = sink.Install(wifiStaNodes.Get(0));
    apps_sink.Start(Seconds(0.5));
    apps_sink.Stop(Seconds(10.0));

    OnOffHelper onoff("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    onoff.SetConstantRate(DataRate("100Mb/s"));
    onoff.SetAttribute("StartTime", TimeValue(Seconds(1.0)));
    onoff.SetAttribute("StopTime", TimeValue(Seconds(10.0)));
    ApplicationContainer apps_source = onoff.Install(wifiApNodes.Get(0));

    /* Set PCAP attributes */
    Config::SetDefault("ns3::PcapFileWrapper::NanosecMode", BooleanValue(true));
    wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
    wifiPhy.EnablePcap("ap", wifiApDevices);
    wifiPhy.EnablePcap("sta", wifiStaDevices);

    /* Start simulation */
    NS_LOG_INFO("Running simulation...");
    Simulator::Stop(Seconds(10.0));
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Finished simulation.");

    return 0;
}