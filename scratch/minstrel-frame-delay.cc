/* Import the required header files */
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/tag.h"
#include "ns3/uinteger.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MinstrelFrameDelay");

/* Define the different tags to store the timestamps */
class AppTxTag : public Tag{
	private:
	Time m_timestamp;

	public:
	static TypeId GetTypeId(void){
		static TypeId tid = TypeId("AppTxTag")
		.SetParent<Tag>()
		.AddConstructor<AppTxTag>()
		.AddAttribute("Timestamp", "A simple timestamp", EmptyAttributeValue(), MakeTimeAccessor(&AppTxTag::GetTimestamp), MakeTimeChecker());
		return tid;
	}

	virtual TypeId GetInstanceTypeId(void) const{
		return GetTypeId();
	}

	virtual uint32_t GetSerializedSize(void) const{
		return 8;
	}

	virtual void Serialize(TagBuffer i) const{
		int64_t t = m_timestamp.GetNanoSeconds();
		i.Write((const uint8_t *)&t, 8);
	}

	virtual void Deserialize(TagBuffer i){
		int64_t t;
		i.Read((uint8_t *)&t, 8);
		m_timestamp = NanoSeconds(t);
	}

	void Print(std::ostream &os) const{
		os << "t=" << m_timestamp;
	}

	void SetTimestamp(Time time){
		m_timestamp = time;
	}

	Time GetTimestamp(void) const{
		return m_timestamp;
	}
};

class MacTxTag : public Tag{
	private:
	Time m_timestamp;

	public:
	static TypeId GetTypeId(void){
		static TypeId tid = TypeId("MacTxTag")
		.SetParent<Tag>()
		.AddConstructor<MacTxTag>()
		.AddAttribute("Timestamp", "A simple timestamp", EmptyAttributeValue(), MakeTimeAccessor(&MacTxTag::GetTimestamp), MakeTimeChecker());
		return tid;
	}

	virtual TypeId GetInstanceTypeId(void) const{
		return GetTypeId();
	}

	virtual uint32_t GetSerializedSize(void) const{
		return 8;
	}

	virtual void Serialize(TagBuffer i) const{
		int64_t t = m_timestamp.GetNanoSeconds();
		i.Write((const uint8_t *)&t, 8);
	}

	virtual void Deserialize(TagBuffer i){
		int64_t t;
		i.Read((uint8_t *)&t, 8);
		m_timestamp = NanoSeconds(t);
	}

	void Print(std::ostream &os) const{
		os << "t=" << m_timestamp;
	}

	void SetTimestamp(Time time){
		m_timestamp = time;
	}

	Time GetTimestamp(void) const{
		return m_timestamp;
	}
};

class PhyTxTag : public Tag{
	private:
	Time m_timestamp;

	public:
	static TypeId GetTypeId(void){
		static TypeId tid = TypeId("PhyTxTag")
		.SetParent<Tag>()
		.AddConstructor<PhyTxTag>()
		.AddAttribute("Timestamp", "A simple timestamp", EmptyAttributeValue(), MakeTimeAccessor(&PhyTxTag::GetTimestamp), MakeTimeChecker());
		return tid;
	}

	virtual TypeId GetInstanceTypeId(void) const{
		return GetTypeId();
	}

	virtual uint32_t GetSerializedSize(void) const{
		return 8;
	}

	virtual void Serialize(TagBuffer i) const{
		int64_t t = m_timestamp.GetNanoSeconds();
		i.Write((const uint8_t *)&t, 8);
	}

	virtual void Deserialize(TagBuffer i){
		int64_t t;
		i.Read((uint8_t *)&t, 8);
		m_timestamp = NanoSeconds(t);
	}

	void Print(std::ostream &os) const{
		os << "t=" << m_timestamp;
	}

	void SetTimestamp(Time time){
		m_timestamp = time;
	}

	Time GetTimestamp(void) const{
		return m_timestamp;
	}
};

class PhyRxTag : public Tag{
	private:
	Time m_timestamp;

	public:
	static TypeId GetTypeId(void){
		static TypeId tid = TypeId("PhyRxTag")
		.SetParent<Tag>()
		.AddConstructor<PhyRxTag>()
		.AddAttribute("Timestamp", "A simple timestamp", EmptyAttributeValue(), MakeTimeAccessor(&PhyRxTag::GetTimestamp), MakeTimeChecker());
		return tid;
	}

	virtual TypeId GetInstanceTypeId(void) const{
		return GetTypeId();
	}

	virtual uint32_t GetSerializedSize(void) const{
		return 8;
	}

	virtual void Serialize(TagBuffer i) const{
		int64_t t = m_timestamp.GetNanoSeconds();
		i.Write((const uint8_t *)&t, 8);
	}

	virtual void Deserialize(TagBuffer i){
		int64_t t;
		i.Read((uint8_t *)&t, 8);
		m_timestamp = NanoSeconds(t);
	}

	void Print(std::ostream &os) const{
		os << "t=" << m_timestamp;
	}

	void SetTimestamp(Time time){
		m_timestamp = time;
	}

	Time GetTimestamp(void) const{
		return m_timestamp;
	}
};

class MacRxTag : public Tag{
	private:
	Time m_timestamp;

	public:
	static TypeId GetTypeId(void){
		static TypeId tid = TypeId("MacRxTag")
		.SetParent<Tag>()
		.AddConstructor<MacRxTag>()
		.AddAttribute("Timestamp", "A simple timestamp", EmptyAttributeValue(), MakeTimeAccessor(&MacRxTag::GetTimestamp), MakeTimeChecker());
		return tid;
	}

	virtual TypeId GetInstanceTypeId(void) const{
		return GetTypeId();
	}

	virtual uint32_t GetSerializedSize(void) const{
		return 8;
	}

	virtual void Serialize(TagBuffer i) const{
		int64_t t = m_timestamp.GetNanoSeconds();
		i.Write((const uint8_t *)&t, 8);
	}

	virtual void Deserialize(TagBuffer i){
		int64_t t;
		i.Read((uint8_t *)&t, 8);
		m_timestamp = NanoSeconds(t);
	}

	void Print(std::ostream &os) const{
		os << "t=" << m_timestamp;
	}

	void SetTimestamp(Time time){
		m_timestamp = time;
	}

	Time GetTimestamp(void) const{
		return m_timestamp;
	}
};

void AppTx(Ptr<const Packet> p){
	AppTxTag timestamp;
	timestamp.SetTimestamp(Simulator::Now());
	p->AddPacketTag(timestamp);
}

void MacTx(Ptr<const Packet> p){
	MacTxTag timestamp;
	timestamp.SetTimestamp(Simulator::Now());
	p->AddPacketTag(timestamp);
}

void PhyTx(Ptr<const Packet> p){
	PhyTxTag timestamp;
	timestamp.SetTimestamp(Simulator::Now());
	p->AddPacketTag(timestamp);
}

void AppRx(Ptr<const Packet> p, const Address &address){
	Time app_rx_timestamp = Simulator::Now();

	AppTxTag app_tx_timestamp;
	MacTxTag mac_tx_timestamp;
	PhyTxTag phy_tx_timestamp;
	PhyRxTag phy_rx_timestamp;
	MacRxTag mac_rx_timestamp;

	p->PeekPacketTag(app_tx_timestamp);
	p->PeekPacketTag(mac_tx_timestamp);
	p->PeekPacketTag(phy_tx_timestamp);
	p->PeekPacketTag(phy_rx_timestamp);
	p->PeekPacketTag(mac_rx_timestamp);

	NS_LOG_INFO(app_tx_timestamp.GetTimestamp() << "," << mac_tx_timestamp.GetTimestamp() << "," << phy_tx_timestamp.GetTimestamp() << "," << phy_rx_timestamp.GetTimestamp() << "," << mac_rx_timestamp.GetTimestamp() << "," << app_rx_timestamp);
}

void MacRx(Ptr<const Packet> p){
	MacRxTag timestamp;
	timestamp.SetTimestamp(Simulator::Now());
	p->AddPacketTag(timestamp);
}
	
void PhyRx(Ptr<const Packet> p){
	PhyRxTag timestamp;
	timestamp.SetTimestamp(Simulator::Now());
	p->AddPacketTag(timestamp);
}

class NodeStatistics{
	public:
	NodeStatistics(NetDeviceContainer aps, NetDeviceContainer stas){

	}

	void checkStatistics(double time){

	}

	void RxCallback(std::string path, Ptr<const Packet> packet, const Address &from){

	}

	void SetPosition(Ptr<Node> node, Vector position){
		Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
		mobility->SetPosition(position);
	}

	Vector GetPosition(Ptr<Node> node){
		Ptr<MobilityModel> mobility = node->GetObject<MobilityModel>();
		return mobility->GetPosition();
	}

	void AdvancePosition(Ptr<Node> node, int stepsSize, int stepsTime){
		Vector pos = GetPosition(node);
		pos.x += stepsSize;
		SetPosition(node, pos);
		//NS_LOG_INFO((Simulator::Now()).GetSeconds()<<" secs. Moving to "<<pos.x<<" metres apart");
		Simulator::Schedule(Seconds(stepsTime), &NodeStatistics::AdvancePosition, this, node, stepsSize, stepsTime);
	}

};

int main(int argc, char *argv[]){
	LogComponentEnable("MinstrelFrameDelay", LOG_INFO);

	GlobalValue::Bind("ChecksumEnabled", BooleanValue(true));

	/* Set the parameters for the simulation */
	uint32_t rtsThreshold = 65535;
	uint32_t BeMaxAmpduSize = 65535;
	bool shortGuardInterval = false;
	uint32_t chWidth = 20;
	int steps = 100;
	int stepsSize = 1;
	int stepsTime = 1;
	int simuTime = steps * stepsSize;

	// Create the Nodes
	NodeContainer wifiApNodes;
	wifiApNodes.Create(1);

	NodeContainer wifiStaNodes;
	wifiStaNodes.Create(1);

	// Create the channel
	YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
	YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
	wifiPhy.SetChannel(wifiChannel.Create());

	WifiHelper wifi;
	WifiMacHelper wifiMac;

	wifi.SetStandard(WIFI_PHY_STANDARD_80211n_2_4GHZ);
	wifi.SetRemoteStationManager("ns3::MinstrelHtWifiManager", "RtsCtsThreshold", UintegerValue(rtsThreshold), "PrintStats", BooleanValue(true));

	Ssid ssid = Ssid("ns3");

	wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));

	// Install the Wifi NetDevices
	NetDeviceContainer wifiApDevices = wifi.Install(wifiPhy, wifiMac, wifiApNodes.Get(0));

	wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));

	NetDeviceContainer wifiStaDevices = wifi.Install(wifiPhy, wifiMac, wifiStaNodes.Get(0));

	NetDeviceContainer wifiDevices;
	wifiDevices.Add(wifiStaDevices);
	wifiDevices.Add(wifiApDevices);

	Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/BE_MaxAmpduSize", UintegerValue(BeMaxAmpduSize));
	Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth", UintegerValue(chWidth));
	Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/ShortGuardIntervalSupported", BooleanValue(shortGuardInterval));

	// Configure the mobility
	MobilityHelper mobility;
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

	positionAlloc->Add(Vector(0.0, 0.0, 0.0));
	positionAlloc->Add(Vector(5.0, 0.0, 0.0));

	mobility.SetPositionAllocator(positionAlloc);

	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

	mobility.Install(wifiApNodes.Get(0));
	mobility.Install(wifiStaNodes.Get(0));

	NodeStatistics atpCounter = NodeStatistics(wifiApDevices, wifiStaDevices);

	Simulator::Schedule(Seconds(0.5 + stepsTime), &NodeStatistics::AdvancePosition, &atpCounter, wifiStaNodes.Get(0), stepsSize, stepsTime);

	InternetStackHelper protocolStack;
	protocolStack.Install(wifiApNodes);
	protocolStack.Install(wifiStaNodes);

	Ipv4AddressHelper address;
	address.SetBase("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer i = address.Assign(wifiDevices);
	Ipv4Address sinkAddress = i.GetAddress(0);
	uint16_t port = 9;

	// Install the applications
	PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(sinkAddress, port));
	ApplicationContainer apps_sink = sink.Install(wifiStaNodes.Get(0));

	OnOffHelper onoff("ns3::UdpSocketFactory", InetSocketAddress(sinkAddress, port));
	onoff.SetConstantRate(DataRate("400Mb/s"), 1420);
	onoff.SetAttribute("StartTime", TimeValue(Seconds(0.5)));
	onoff.SetAttribute("StopTime", TimeValue(Seconds(simuTime)));
	ApplicationContainer apps_source = onoff.Install(wifiApNodes.Get(0));

	apps_sink.Start(Seconds(0.5));
	apps_sink.Stop(Seconds(simuTime));

	Config::Connect("/NodeList/1/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback(&NodeStatistics::RxCallback, &atpCounter));

	// Connect the callbacks
	Config::ConnectWithoutContext("/NodeList/*/ApplicationList/*/$ns3::OnOffApplication/Tx", MakeCallback(&AppTx));
	Config::ConnectWithoutContext("/NodeList/*/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback(&AppRx));
	Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyTxBegin", MakeCallback(&PhyTx));
	Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxBegin", MakeCallback(&PhyRx));
	Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacTx", MakeCallback(&MacTx));
	Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Mac/MacRx", MakeCallback(&MacRx));
	
	/* Enable PCAP */
	Config::SetDefault("ns3::PcapFileWrapper::NanosecMode", BooleanValue(true));
	wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
	wifiPhy.EnablePcap("ap-minstrel-inc", wifiApDevices.Get(0));
	wifiPhy.EnablePcap("sta-minstrel-inc", wifiStaDevices.Get(0));
	
	// Get Mac object so that SIFS, Slot time can be printed
	/*
	Ptr<WifiNetDevice> wifiApDevice = DynamicCast<WifiNetDevice>(wifiApDevices.Get(0));
	Ptr<WifiNetDevice> wifiStaDevice = DynamicCast<WifiNetDevice>(wifiStaDevices.Get(0));

	Ptr<WifiMac> wifiApMac = wifiApDevice->GetMac();
	Ptr<WifiMac> wifiStaMac = wifiStaDevice->GetMac();

	std::cout<<wifiApMac->GetSifs()<<std::endl;
	std::cout<<wifiStaMac->GetSifs()<<std::endl;
	std::cout<<wifiApMac->GetSlot()<<std::endl;
	std::cout<<wifiStaMac->GetSlot()<<std::endl;
	std::cout<<wifiApMac->GetShortSlotTimeSupported()<<std::endl;
	std::cout<<wifiStaMac->GetShortSlotTimeSupported()<<std::endl;
	*/

	Simulator::Stop(Seconds(simuTime));
	Simulator::Run();

	Simulator::Destroy();

	return 0;
}
