#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("SimpleWifi");

int main(int argc, char *argv[]){
    /* Set nanosecond resolution */
    Time::SetResolution(Time::NS);

    /* Enable log messages */
    LogComponentEnable("SimpleWifi", LOG_ALL);

    /* Enable checksums */
    GlobalValue::Bind("ChecksumEnabled", BooleanValue(true));

    /* Create Nodes */
    NS_LOG_INFO("Creating the wifi nodes");
    NodeContainer wifiApNodes;
    wifiApNodes.Create(1);
    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(1);

    /* Create the wifi channel */
    NS_LOG_INFO("Creating the wifi channel");
    WifiHelper wifi;
    wifi.SetStandard(WIFI_PHY_STANDARD_80211n_2_4GHZ);
    wifi.SetRemoteStationManager("ns3::MinstrelHtWifiManager");

    YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();

    /* Setting up the mobility */
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();
    positionAlloc->Add(Vector(0.0, 0.0, 1.0));
    positionAlloc->Add(Vector(30.0, 0.0, 1.0));
    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiApNodes);
    mobility.Install(wifiStaNodes);

    wifiPhy.SetChannel(wifiChannel.Create());

    WifiMacHelper wifiMac;
    Ssid ssid = Ssid("ns3");

    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiApDevices = wifi.Install(wifiPhy, wifiMac, wifiApNodes.Get(0));

    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiStaDevices = wifi.Install(wifiPhy, wifiMac, wifiStaNodes.Get(0));

    /* Install the internet stack */
    NS_LOG_INFO("Installing the internet stack");
    InternetStackHelper internet;
    internet.Install(wifiApNodes);
    internet.Install(wifiStaNodes);

    /* Assigning the IP addresses */
    NS_LOG_INFO("Assigning the IP addresses");
    Ipv4AddressHelper address;
    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiApInterfaces = address.Assign(wifiApDevices);
    Ipv4InterfaceContainer wifiStaInterfaces = address.Assign(wifiStaDevices);

    /* Install the applications */
    NS_LOG_INFO("Installing the applications");
    uint16_t port = 9;
    PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    ApplicationContainer apps_sink = sink.Install(wifiStaNodes.Get(0));
    apps_sink.Start(Seconds(0.0));
    apps_sink.Stop(Seconds(2.0));

    OnOffHelper onoff("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    onoff.SetConstantRate(DataRate("100Mb/s"));
    onoff.SetAttribute("StartTime", TimeValue(Seconds(0.5)));
    onoff.SetAttribute("StopTime", TimeValue(Seconds(2.0)));
    ApplicationContainer apps_source = onoff.Install(wifiApNodes.Get(0));

    Config::SetDefault("ns3::PcapFileWrapper::NanosecMode", BooleanValue(true));
    wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
    wifiPhy.EnablePcap("ap", wifiApDevices);
    wifiPhy.EnablePcap("sta", wifiStaDevices);

    /* Start the simulation */
    NS_LOG_INFO("Running the simulation...");
    Simulator::Stop(Seconds(2.0));
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Simulation finished");

    return 0;
}